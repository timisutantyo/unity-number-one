﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

    public GameObject projectile;
    public float fireRate = 0.5F;
    private float nextFire = 0.0F;
    
    void Update () {

        if (Input.GetButton("Fire1"))
        {

            Vector3 playerPos = transform.position;
            Vector3 playerDirection = transform.forward;
            Quaternion playerRotation = transform.rotation;

            Vector3 spawnPos = playerPos + playerDirection * 3;
            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
                GameObject bullet = Instantiate(projectile, spawnPos, playerRotation);
                bullet.GetComponent<Projectile>().Fire();

            }
        }

	}
}
