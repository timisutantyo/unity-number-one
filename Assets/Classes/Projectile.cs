﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    private Rigidbody rb;
    public float force;
    public int damage = 2;

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    public void Fire()
    {
        rb.AddForce(transform.forward * force);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Musuh")
        {
            EnemyController enemy = collision.gameObject.GetComponent<EnemyController>();
            enemy.GetHit(damage);
        }
    }
}
